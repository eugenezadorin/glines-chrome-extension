# Glines Chrome Extension

Simple tool for easy markup testing with guide lines.

## Demo 

![Glines demo](http://i.imgur.com/8Yt99WG.gif)

## How to install

1. Download or clone this repository
2. Open chrome://extensions/ page and turn on "developer mode"
3. Click on "Load Unpacked Extension" button and navigate to glines extension folder.

## How to use

1. Click on plugin button at Chrome panel
2. Use dropdown menu to add lines
3. Move lines using mouse or keyboard arrows
4. Drag lines with Ctrl to clone them
5. Delete lines by double click, or remove all using menu

## Crosslines

1. Use dropdown menu to add crossline
2. Use left mouse button to fix crossline
3. Use right mouse button to remove crossline